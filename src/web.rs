use crate::common::*;
use crate::config::HedwigConfig;
use crate::relay_handler::RelayHandler;
use actix::*;
use actix_web::web::Data;
use actix_web::{web::Payload, Error, HttpRequest, HttpResponse};
use actix_web_actors::ws;
use actix_web_actors::ws::WebsocketContext;
use log::{debug, info};
use rand::{thread_rng, Rng};
use std::result::Result as StdResult;
use std::time::Duration;
use std::time::Instant;

pub struct HedwigState {
    pub relay_handler: Addr<RelayHandler>,
    pub config: HedwigConfig,
}

pub struct SubscriptionSession {
    pub relay_handler: Addr<RelayHandler>,
    pub client_addr: String,
    pub subscription_id: Option<SubscriptionId>,
    pub client_heartbeat: Instant,
    pub heartbeat: Instant,
}

impl SubscriptionSession {
    fn start_heartbeat(&self, ctx: &mut <Self as Actor>::Context) {
        ctx.run_interval(Duration::from_secs(60), |_, ctx| {
            let heartbeat: i64 = thread_rng().gen::<i64>();
            ctx.ping(&heartbeat.to_le_bytes());
        });
    }
}

impl Actor for SubscriptionSession {
    type Context = WebsocketContext<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        info!("Starting subscription for {:?}", self.client_addr);

        let addr: Addr<SubscriptionSession> = ctx.address();
        self.relay_handler
            .send(AddSubscriber { subscriber: addr })
            .into_actor(self)
            .then(|res: StdResult<SubscriptionResponse, _>, mut act, ctx| {
                match res {
                    Ok(res) => {
                        act.subscription_id = Some(res.subscription_id);

                        info!(
                            "Sending scrollback for {:?}, {} items",
                            res.subscription_id,
                            res.scrollback.len()
                        );

                        res.scrollback
                            .into_iter()
                            .map(|msg| serde_json::to_string(&msg).unwrap())
                            .for_each(|msg| ctx.text(msg));

                        act.start_heartbeat(ctx);
                    }
                    // something is wrong, abort.
                    _ => ctx.stop(),
                }
                fut::ready(())
            })
            .wait(ctx);
    }

    fn stopping(&mut self, _ctx: &mut <Self as Actor>::Context) -> Running {
        if let Some(subscription_id) = self.subscription_id {
            self.relay_handler
                .do_send(RemoveSubscriber { subscription_id });
        }

        Running::Stop
    }
}

/// Handle messages from chat server, we simply send it to peer websocket
impl Handler<RelayMessage> for SubscriptionSession {
    type Result = ();

    fn handle(&mut self, msg: RelayMessage, ctx: &mut Self::Context) {
        ctx.text(serde_json::to_string(&msg).unwrap());
    }
}

impl StreamHandler<StdResult<ws::Message, ws::ProtocolError>> for SubscriptionSession {
    fn handle(&mut self, msg: StdResult<ws::Message, ws::ProtocolError>, ctx: &mut Self::Context) {
        debug!("WEBSOCKET MESSAGE: {:?}", msg);
        match msg.expect("Expected websocket message") {
            ws::Message::Ping(msg) => ctx.pong(&msg),
            ws::Message::Pong(_) => self.heartbeat = Instant::now(),
            ws::Message::Text(msg) => {
                if msg == "ping" {
                    self.client_heartbeat = Instant::now();
                }
            }
            ws::Message::Binary(_) => {}
            ws::Message::Close(_) => {
                ctx.stop();
            }
            ws::Message::Nop => {}
            _ => {}
        }
    }
}

pub async fn chat_route(req: HttpRequest, stream: Payload) -> StdResult<HttpResponse, Error> {
    let client_addr = req
        .connection_info()
        .realip_remote_addr()
        .unwrap_or("<unknown>")
        .to_string();

    let state: &HedwigState = req.app_data::<Data<HedwigState>>().unwrap();
    ws::start(
        SubscriptionSession {
            relay_handler: state.relay_handler.clone(),
            client_addr,
            subscription_id: None,
            client_heartbeat: Instant::now(),
            heartbeat: Instant::now(),
        },
        &req,
        stream,
    )
}
