use actix::prelude::*;
use actix_web::web as actixweb;
use actix_web::App;
use actix_web::HttpServer;
use log::info;
use sqlx::postgres::PgPoolOptions;

use crate::config::HedwigConfig;
use crate::relay_handler::RelayHandlerBuilder;

mod common;
mod config;
mod discord;
mod relay_handler;
mod web;

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();

    let config: HedwigConfig = config::read_configuration().expect("couldn't read configuration");

    let dbpool = PgPoolOptions::new()
        .max_connections(config.database().max_connections())
        .connect(config.database().url())
        .await
        .unwrap();

    let relay_handler = RelayHandlerBuilder::builder()
        .scrollback_capacity(100)
        .build();

    let (ready_tx, ready_rx) = tokio::sync::oneshot::channel::<()>();
    let relay_handler = relay_handler.start();
    {
        let config = config.clone();
        let relay_handler = relay_handler.clone();
        tokio::spawn(async move {
            discord::start(config, dbpool, relay_handler, ready_tx)
                .await
                .expect("Failed to open Discord client");
        })
    };

    // Await client startup.
    ready_rx.await.unwrap();

    let listen_addr = config.listen_addr().to_owned().clone();
    info!("Starting server on {}", listen_addr);
    HttpServer::new(move || {
        // Websocket sessions state
        let state = web::HedwigState {
            relay_handler: relay_handler.clone(),
            config: config.clone(),
        };

        App::new()
            .data(state)
            // websocket
            .service(actixweb::resource("/ws/").route(actixweb::get().to(web::chat_route)))
    })
    .shutdown_timeout(5)
    .bind(listen_addr)?
    .run()
    .await
}
