use crate::common::Result;
use crate::config::HedwigConfig;
use crate::discord::event_handler::DiscordEventHandler;
use crate::discord::quotes::QuoteStorage;
use crate::relay_handler::RelayHandler;
use actix::Addr;
use log::{info, warn};
use serenity::client::bridge::gateway::GatewayIntents;
use serenity::framework::{
    standard::{macros::hook, CommandError, DispatchError},
    StandardFramework,
};
use serenity::model::channel::Message;
use serenity::prelude::*;
use sqlx::PgPool;
use std::result::Result as StdResult;
use tokio::sync::oneshot::Sender;

pub mod channel;
mod event_handler;
mod logging;
mod moderation;
mod quotes;
pub mod relay;
mod retention;
pub mod storage;
mod utils;

pub struct HedwigState {
    pub config: HedwigConfig,
    pub dbpool: PgPool,
}

impl TypeMapKey for HedwigState {
    type Value = HedwigState;
}

pub async fn start(
    config: HedwigConfig,
    dbpool: PgPool,
    relay_handler: Addr<RelayHandler>,
    ready_tx: Sender<()>,
) -> Result<()> {
    info!("Starting Discord client...");
    let event_handler = DiscordEventHandler::new(config.clone(), relay_handler.clone(), ready_tx);

    // Login with a bot token from the environment
    let mut framework = StandardFramework::new()
        .configure(|c| c.prefix("!"))
        .before(before_hook)
        .after(after_hook)
        .on_dispatch_error(dispatch_error_hook);

    if !config.test_mode() {
        framework = framework.group(&quotes::QUOTES_GROUP);
    }

    let mut client = Client::builder(config.discord_token())
        .application_id(config.application_id())
        .event_handler(event_handler)
        .framework(framework)
        .intents(GatewayIntents::non_privileged() | GatewayIntents::GUILD_MEMBERS)
        .await?;

    {
        let mut data = client.data.write().await;
        let quote_storage = QuoteStorage::from_file("quotes.json")?;
        data.insert::<QuoteStorage>(quote_storage);
        data.insert::<HedwigState>(HedwigState { config, dbpool });
    }

    client.cache_and_http.cache.set_max_messages(1000).await;

    // start listening for events by starting a single shard
    client.start().await?;

    Ok(())
}

#[hook]
async fn before_hook(_ctx: &Context, msg: &Message, command_name: &str) -> bool {
    info!(
        "Received command '{}' by user '{}'",
        command_name, msg.author.name
    );
    true
}

#[hook]
async fn after_hook(
    ctx: &Context,
    msg: &Message,
    command_name: &str,
    error: StdResult<(), CommandError>,
) {
    match error {
        Ok(()) => info!("Processed command '{}'", command_name),
        Err(why) => {
            warn!("Command '{}' returned error {:?}", command_name, why);
            let _ = msg
                .channel_id
                .say(&ctx, "Command failed, see log for details.");
        }
    }
}

#[hook]
async fn dispatch_error_hook(ctx: &Context, msg: &Message, error: DispatchError) {
    match error {
        DispatchError::Ratelimited(seconds) => {
            let _ = msg.channel_id.say(
                &ctx,
                &format!("Try this again in {} seconds.", seconds.as_secs()),
            );
        }
        DispatchError::LackingRole => {
            let _ = msg
                .channel_id
                .say(&ctx, "You're missing the roles to execute this command.");
        }
        _ => {}
    }
}
