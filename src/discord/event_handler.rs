use actix::Addr;
use log::{error, info, warn};
use serenity::model::prelude::*;
use serenity::prelude::*;
use std::cell::RefCell;

use tokio::sync::oneshot::Sender;

use crate::common::RelayScrollback;
use crate::config::HedwigConfig;
use crate::discord::logging::LoggingHandler;
use crate::discord::moderation;
use crate::discord::retention;
use crate::discord::storage::message::StoredMessage;
use crate::discord::utils::ErrorExt;
use crate::discord::{relay, HedwigState};
use crate::relay_handler::RelayHandler;

use tokio::task::JoinHandle;

pub struct DiscordEventHandler {
    config: HedwigConfig,
    logging_handler: LoggingHandler,
    relay_handler: Addr<RelayHandler>,
    ready_tx: Mutex<RefCell<Option<Sender<()>>>>,
    tasks: Mutex<Vec<JoinHandle<()>>>,
}

impl DiscordEventHandler {
    pub fn new(
        config: HedwigConfig,
        relay_handler: Addr<RelayHandler>,
        ready_tx: Sender<()>,
    ) -> DiscordEventHandler {
        DiscordEventHandler {
            config: config.clone(),
            logging_handler: LoggingHandler::new(config),
            relay_handler,
            ready_tx: Mutex::new(RefCell::new(Some(ready_tx))),
            tasks: Mutex::new(Vec::new()),
        }
    }

    fn should_relay(&self, message: &Message) -> bool {
        self.config.relay().relay_channel() == message.channel_id
    }
}

#[serenity::async_trait]
impl EventHandler for DiscordEventHandler {
    async fn message(&self, ctx: Context, message: Message) {
        if self.should_relay(&message) {
            let relay_message = relay::to_relay_message(
                &ctx,
                &message.author,
                &message.guild_id,
                &message.content,
                &message.timestamp,
            )
            .await;
            if let Some(message) = &relay_message {
                self.relay_handler
                    .try_send(message.clone())
                    .expect("Failed to send!")
            }
        }

        if let Err(e) = self.logging_handler.message_created(&ctx, &message).await {
            warn!("Failed to handle message creation: {:?}", e);
        }
    }

    async fn message_delete(
        &self,
        ctx: Context,
        channel_id: ChannelId,
        deleted_message_id: MessageId,
        _guild_id: Option<GuildId>,
    ) {
        self.logging_handler
            .message_deleted(ctx, channel_id, deleted_message_id)
            .await
            .if_err(|e| warn!("Failed to handle message update: {:?}", e));
    }

    async fn message_update(
        &self,
        ctx: Context,
        _old_message: Option<Message>,
        _new_message: Option<Message>,
        event: MessageUpdateEvent,
    ) {
        if let Err(e) = self.logging_handler.message_updated(ctx, &event).await {
            warn!(
                "Failed to handle message update: {:?}, for update: {:?}",
                e, event
            );
        }
    }

    async fn ready(&self, ctx: Context, ready: Ready) {
        if let Err(e) = self.logging_handler.load_history(&ctx).await {
            warn!("Failed to load history: {:?}", e);
        }

        // Load scrollback into relay cache.
        if let Err(e) = load_scrollback(&self.config, &ctx, self.relay_handler.clone()).await {
            warn!("Failed to load scrollback: {:?}", e);
        }

        // Notify main thread we're ready.
        let ready_tx = self.ready_tx.lock().await.replace(None).unwrap();
        ready_tx.send(()).expect("Unable to signal readiness");

        // Start polling work.
        let data = ctx.data.read().await;
        self.start_periodic_work(&ctx, data.get::<HedwigState>().unwrap())
            .await;

        for guild in &ready.guilds {
            info!("Registering interactions for guild: {}", guild.id());
            match moderation::register_interactions(&ctx, guild.id()).await {
                Ok(_) => {}
                Err(e) => error!("Failed to register interactions: {:?}", e),
            }
        }

        info!(
            "Connected as {} to {} guilds.",
            ready.user.name,
            ready.guilds.len()
        );
    }

    async fn resume(&self, ctx: Context, _: ResumedEvent) {
        if let Err(e) = self.logging_handler.load_history(&ctx).await {
            warn!("Failed to load history: {:?}", e);
        }
    }

    async fn interaction_create(&self, ctx: Context, interaction: Interaction) {
        match interaction.kind {
            InteractionType::Ping => {}
            InteractionType::ApplicationCommand => {
                let data = match interaction.data.as_ref().unwrap() {
                    InteractionData::ApplicationCommand(data) => data,
                    InteractionData::MessageComponent(_) => return,
                };

                match data.name.as_str() {
                    "jail" => moderation::jail_interaction(&ctx, &interaction)
                        .await
                        .if_err(|e| {
                            error!("Failed to jail: {:?}", e);
                        }),
                    "unjail" => moderation::unjail_interaction(&ctx, &interaction)
                        .await
                        .if_err(|e| {
                            error!("Failed to unjail: {:?}", e);
                        }),
                    "role" => moderation::role_interaction(&ctx, &interaction)
                        .await
                        .if_err(|e| {
                            error!("Role command failed: {:?}", e);
                        }),
                    cmd => warn!("Unhandled command: {}", cmd),
                }
            }
            InteractionType::Unknown => {}
            _ => {}
        };
    }
}

impl DiscordEventHandler {
    async fn start_periodic_work(&self, ctx: &Context, state: &HedwigState) {
        let retention_poller = tokio::spawn({
            let ctx = ctx.clone();
            let config = state.config.clone();
            let dbpool = state.dbpool.clone();
            async move { retention::run_periodic_work(ctx, config, dbpool).await }
        });
        let moderation_poller = tokio::spawn({
            let ctx = ctx.clone();
            let config = state.config.clone();
            let dbpool = state.dbpool.clone();
            async move { moderation::run_periodic_work(ctx, config, dbpool).await }
        });

        let mut tasks = self.tasks.lock().await;
        tasks.push(retention_poller);
        tasks.push(moderation_poller);
    }
}

// TODO: move to relay_handler once that's de-actix'd.
pub async fn load_scrollback(
    config: &HedwigConfig,
    ctx: &Context,
    relay_handler: Addr<RelayHandler>,
) -> std::result::Result<(), failure::Error> {
    if config.test_mode() {
        return Ok(());
    }

    let data = ctx.data.read().await;
    let state: &HedwigState = data.get::<HedwigState>().unwrap();

    // TODO: set the scrollback rather than sending N messages, then this can be used from resume()
    let messages = StoredMessage::latest_messages_for_scrollback(
        &state.dbpool,
        config.relay().relay_channel(),
        100,
    )
    .await?;

    let mut relay_messages = Vec::new();
    for message in messages.iter().rev() {
        let user: User = message.author().to_user(&ctx).await?;

        if let Some(message) = relay::to_relay_message(
            &ctx,
            &user,
            &Some(message.guild()),
            &message.content,
            &message.created_at,
        )
        .await
        {
            relay_messages.push(message);
        }
    }

    relay_handler.do_send(RelayScrollback(relay_messages));

    Ok(())
}
