use serenity::client::Context;
use serenity::http::CacheHttp;
use serenity::model::channel::Message;
use serenity::model::id::GuildId;
use serenity::model::user::User;

pub fn escape_markdown(content: &str) -> String {
    content
        .replace("|", "\\|")
        .replace("*", "\\*")
        .replace("_", "\\_")
        .replace("~", "\\~")
}

pub async fn formatted_message_name(context: &Context, msg: &Message) -> String {
    formatted_name(context, &msg.guild_id.unwrap(), &msg.author).await
}

pub async fn formatted_name(
    context: impl CacheHttp,
    guild_id: impl Into<GuildId>,
    user: &User,
) -> String {
    if let Some(nick) = user.nick_in(&context, guild_id.into()).await {
        format!("{} ({})", user.tag(), nick)
    } else {
        user.tag()
    }
}

pub trait ErrorExt<E, F>
where
    F: FnOnce(&E),
{
    fn if_err(&self, error_callback: F);
}

impl<R, E, F> ErrorExt<E, F> for Result<R, E>
where
    F: FnOnce(&E),
{
    fn if_err(&self, error_callback: F) {
        if self.is_err() {
            error_callback(self.as_ref().err().unwrap());
        }
    }
}
