use chrono::{DateTime, Utc};
use serde_derive::Serialize;
use serenity::client::Context;
use serenity::model::channel::Message;
use serenity::model::event::MessageUpdateEvent;
use serenity::model::id::{ChannelId, GuildId, MessageId, UserId};
use sqlx::postgres::PgRow;
use sqlx::{PgPool, Row};

use crate::discord::storage::user::StoredUser;
use crate::discord::storage::SqlResult;

#[derive(Debug, Serialize, sqlx::FromRow)]
pub struct StoredMessage {
    pub message_id: i64,
    pub channel_id: i64,
    pub guild_id: i64,
    pub author_id: i64,
    pub content: String,
    pub created_at: DateTime<Utc>,
    pub edited_at: Option<DateTime<Utc>>,
    pub deleted_at: Option<DateTime<Utc>>,
}

impl StoredMessage {
    pub async fn latest_message_id(
        pool: &PgPool,
        channel_id: &ChannelId,
    ) -> SqlResult<Option<MessageId>> {
        sqlx::query("SELECT MAX(message_id) FROM messages WHERE channel_id = $1")
            .bind(channel_id.0 as i64)
            .try_map(|row: PgRow| row.try_get::<i64, _>(0).map(|id| MessageId(id as u64)))
            .fetch_optional(pool)
            .await
    }

    pub async fn latest_messages_for_scrollback(
        pool: &PgPool,
        channel_id: ChannelId,
        messages: u32,
    ) -> SqlResult<Vec<StoredMessage>> {
        sqlx::query_as::<_, StoredMessage>(
            "SELECT * \
            FROM messages \
            WHERE channel_id = $1 \
            AND deleted_at IS NULL \
            AND content != '' \
            ORDER BY created_at DESC \
            LIMIT $2",
        )
        .bind(*channel_id.as_u64() as i64)
        .bind(messages)
        .fetch_all(pool)
        .await
    }

    pub async fn newest_before_horizon(
        pool: &PgPool,
        channel_id: &ChannelId,
        horizon: DateTime<Utc>,
    ) -> SqlResult<Option<MessageId>> {
        sqlx::query(
            "SELECT MIN(message_id) FROM messages WHERE channel_id = $1 AND created_at > $2",
        )
        .bind(channel_id.0 as i64)
        .bind(horizon)
        .try_map(|row: PgRow| {
            row.try_get::<Option<i64>, _>(0)
                .map(|id_opt| id_opt.map(|id| MessageId(id as u64)))
        })
        .fetch_one(pool)
        .await
    }

    pub async fn fetch(pool: &PgPool, message_id: &MessageId) -> SqlResult<Option<StoredMessage>> {
        sqlx::query_as::<_, StoredMessage>("SELECT * FROM messages WHERE message_id = $1")
            .bind(*message_id.as_u64() as i64)
            .fetch_optional(pool)
            .await
    }

    pub async fn insert(pool: &PgPool, context: &Context, message: &Message) -> SqlResult<()> {
        StoredUser::insert(pool, context, &message.author, message.guild_id.unwrap()).await?;
        sqlx::query(
            r#"
        INSERT INTO messages (message_id, channel_id, guild_id, author_id, content, created_at, edited_at)
        VALUES ( $1, $2, $3, $4, $5, $6, $7 )
        ON CONFLICT DO NOTHING"#)
        .bind(message.id.0 as i64)
        .bind(message.channel_id.0 as i64)
        .bind(message.guild_id.unwrap().0 as i64)
        .bind(message.author.id.0 as i64)
        .bind(&message.content)
        .bind(message.timestamp)
        .bind(message.edited_timestamp)
        .execute(pool)
        .await?;

        Ok(())
    }

    pub async fn edit(&self, pool: &PgPool, update: &MessageUpdateEvent) -> SqlResult<()> {
        sqlx::query(
            r#"
        UPDATE messages
        SET content = $1, edited_at = $2
        WHERE message_id = $3
        "#,
        )
        .bind(&update.content)
        .bind(Utc::now())
        .bind(self.message_id)
        .execute(pool)
        .await?;

        Ok(())
    }

    pub async fn soft_delete(&self, pool: &PgPool) -> SqlResult<()> {
        sqlx::query(
            r#"
        UPDATE messages
        SET deleted_at = $1
        WHERE message_id = $2
        "#,
        )
        .bind(Utc::now())
        .bind(self.message_id)
        .execute(pool)
        .await?;

        Ok(())
    }

    pub fn author(&self) -> UserId {
        UserId(self.author_id as u64)
    }

    pub fn guild(&self) -> GuildId {
        GuildId(self.guild_id as u64)
    }
}
