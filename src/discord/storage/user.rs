use chrono::{DateTime, Utc};
use serde_derive::Serialize;
use serenity::client::Context;
use serenity::model::id::GuildId;
use serenity::model::prelude::User;
use sqlx::{Done, PgPool};

use crate::discord::storage::SqlResult;

#[derive(Debug, Serialize, sqlx::FromRow)]
pub struct StoredUser {
    pub id: i64,
    pub name: String,
    pub discriminator: i16,
    pub guild_id: i64,
    pub nickname: Option<String>,
    pub created_at: DateTime<Utc>,
}

impl StoredUser {
    pub async fn insert(
        pool: &PgPool,
        context: &Context,
        user: &User,
        guild_id: GuildId,
    ) -> SqlResult<()> {
        let nickname = user.nick_in(&context, guild_id).await;
        let now = Utc::now();

        let created = {
            sqlx::query(
                r#"
        INSERT INTO users (id, name, discriminator, guild_id, nickname, created_at)
        VALUES ( $1, $2, $3, $4, $5, $6 )
        ON CONFLICT DO NOTHING"#,
            )
            .bind(user.id.0 as i64)
            .bind(&user.name)
            .bind(user.discriminator as i32)
            .bind(guild_id.0 as i64)
            .bind(&nickname)
            .bind(now)
            .execute(pool)
            .await?
        };

        if created.rows_affected() > 0 {
            sqlx::query(
                r#"
        INSERT INTO user_history (user_id, name, discriminator, guild_id, nickname, created_at)
        VALUES ( $1, $2, $3, $4, $5, $6 )"#,
            )
            .bind(user.id.0 as i64)
            .bind(&user.name)
            .bind(user.discriminator as i32)
            .bind(guild_id.0 as i64)
            .bind(&nickname)
            .bind(now)
            .execute(pool)
            .await?;
        }

        Ok(())
    }
}
