use chrono::{DateTime, Utc};
use serde_derive::Serialize;

use serenity::model::id::{GuildId, RoleId, UserId};
use serenity::model::prelude::User;
use sqlx::{PgPool, Row};

use crate::discord::storage::SqlResult;
use serenity::model::guild::Member;
use sqlx::postgres::PgDone;

#[derive(Debug, Serialize, sqlx::FromRow)]
pub struct TemporaryRole {
    pub user_id: i64,
    pub guild_id: i64,
    pub role_id: i64,
    pub actor_user_id: i64,
    pub remove_at: DateTime<Utc>,
    pub created_at: DateTime<Utc>,
}

impl TemporaryRole {
    pub(crate) fn user_id(&self) -> UserId {
        UserId(self.user_id as u64)
    }
    pub(crate) fn guild_id(&self) -> GuildId {
        GuildId(self.guild_id as u64)
    }
    pub(crate) fn role_id(&self) -> RoleId {
        RoleId(self.role_id as u64)
    }
}

impl TemporaryRole {
    pub async fn list_expired_roles(
        pool: &PgPool,
        horizon: DateTime<Utc>,
    ) -> SqlResult<Vec<TemporaryRole>> {
        sqlx::query_as::<_, TemporaryRole>("SELECT * FROM temporary_roles WHERE remove_at <= $1")
            .bind(horizon)
            .fetch_all(pool)
            .await
    }

    pub async fn roles_for_user(
        pool: &PgPool,
        user: UserId,
        guild: GuildId,
    ) -> SqlResult<Vec<TemporaryRole>> {
        sqlx::query_as::<_, TemporaryRole>(
            "SELECT * FROM temporary_roles WHERE user_id = $1 AND guild_id = $2",
        )
        .bind(user.0 as i64)
        .bind(guild.0 as i64)
        .fetch_all(pool)
        .await
    }

    pub async fn role_by_user(
        pool: &PgPool,
        user: UserId,
        guild: GuildId,
        role: RoleId,
    ) -> SqlResult<Option<TemporaryRole>> {
        sqlx::query_as::<_, TemporaryRole>(
            "SELECT * FROM temporary_roles WHERE user_id = $1 AND guild_id = $2 AND role_id = $3",
        )
        .bind(user.0 as i64)
        .bind(guild.0 as i64)
        .bind(role.0 as i64)
        .fetch_optional(pool)
        .await
    }

    pub async fn remove(
        pool: &PgPool,
        user: UserId,
        guild: GuildId,
        role: RoleId,
    ) -> SqlResult<PgDone> {
        sqlx::query(
            "DELETE FROM temporary_roles WHERE user_id = $1 AND guild_id = $2 AND role_id = $3",
        )
        .bind(user.0 as i64)
        .bind(guild.0 as i64)
        .bind(role.0 as i64)
        .execute(pool)
        .await
    }

    pub async fn insert(
        pool: &PgPool,
        user: &Member,
        role: RoleId,
        actor: &User,
        remove_at: &DateTime<Utc>,
    ) -> SqlResult<()> {
        let now = Utc::now();

        sqlx::query(
            r#"
        INSERT INTO temporary_roles (user_id, guild_id, role_id, actor_user_id, remove_at, created_at)
        VALUES ( $1, $2, $3, $4, $5, $6 )
        "#)
            .bind(user.user.id.0 as i64)
            .bind(user.guild_id.0 as i64)
            .bind(role.0 as i64)
            .bind(actor.id.0 as i64)
            .bind(remove_at)
            .bind(now)
        .execute(pool)
        .await?;

        Ok(())
    }

    pub async fn update(
        pool: &PgPool,
        user: &Member,
        role: RoleId,
        _actor: &User,
        remove_at: &DateTime<Utc>,
    ) -> SqlResult<()> {
        let _now = Utc::now();

        sqlx::query(
            r#"
        UPDATE temporary_roles SET remove_at = $4 WHERE user_id = $1 AND guild_id = $2 AND role_id = $3
        "#)
            .bind(user.user.id.0 as i64)
            .bind(user.guild_id.0 as i64)
            .bind(role.0 as i64)
            .bind(remove_at)
        .execute(pool)
        .await?;

        Ok(())
    }

    pub fn is_expired(&self, horizon: DateTime<Utc>) -> bool {
        self.remove_at < horizon
    }
}
