pub mod message;
pub mod temporary_role;
pub mod user;

pub type SqlResult<T> = Result<T, sqlx::Error>;
