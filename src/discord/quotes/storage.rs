use chrono::{DateTime, Utc};
use rand::{thread_rng, Rng};
use regex::Regex;
use serde::{Deserialize, Serialize};
use serenity::prelude::TypeMapKey;
use std::collections::vec_deque::VecDeque;
use std::collections::HashMap;
use std::fs::{File, OpenOptions};
use std::io::{BufRead, BufReader, Write};
use std::path::PathBuf;

use crate::discord::utils;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct EditHistory {
    pub date: DateTime<Utc>,
    pub content: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Quote {
    pub id: usize,
    pub date: DateTime<Utc>,
    pub submitter: String,
    pub content: String,
    #[serde(default)]
    pub edit_history: Vec<EditHistory>,
    pub edited_by: Option<String>,
    pub edited_at: Option<DateTime<Utc>>,
}

impl Quote {
    fn parse_from_line(line: String) -> Result<Quote, failure::Error> {
        serde_json::from_str(&line).map_err(|e| e.into())
    }

    pub fn added_on_formatted(&self) -> String {
        self.date.format("%a %b %d %T %z %Y").to_string()
    }

    pub fn edited_on_formatted(&self) -> Option<String> {
        self.edited_at
            .map(|d| d.format("%a %b %d %T %z %Y").to_string())
    }

    pub fn escaped_content(&self) -> String {
        utils::escape_markdown(&self.content)
    }
}

pub struct QuoteStorage {
    file: File,
    file_path: String,
    id_to_quote: HashMap<usize, Quote>,
    max_quote_id: usize,
    quote_memory: RecentQuoteMemory,
}

impl QuoteStorage {
    pub fn from_file(path: &str) -> Result<QuoteStorage, failure::Error> {
        let file = OpenOptions::new()
            .read(true)
            .write(true)
            .open(PathBuf::from(path))?;
        let reader = BufReader::new(&file);

        let id_to_quote: HashMap<usize, Quote> = reader
            .lines()
            .filter_map(|l| l.ok())
            .map(Quote::parse_from_line)
            .filter_map(|l| l.ok())
            .map(|q| (q.id, q))
            .collect();
        let max_quote_id = id_to_quote.values().map(|q| q.id).max().unwrap_or(0);

        Ok(QuoteStorage {
            file,
            file_path: path.to_string(),
            id_to_quote,
            max_quote_id,
            quote_memory: RecentQuoteMemory::new(5),
        })
    }

    pub fn get_quote(&self, id: usize) -> Option<&Quote> {
        self.id_to_quote.get(&id)
    }

    pub fn del_quote(&mut self, id: usize) -> Result<Option<Quote>, failure::Error> {
        let quote = self.id_to_quote.remove(&id);
        match self.rewrite_storage() {
            Ok(_) => Ok(quote),
            Err(e) => {
                // Reinsert on failure to rewrite.
                if let Some(quote) = quote {
                    self.id_to_quote.insert(id, quote);
                }

                Err(e.into())
            }
        }
    }

    pub fn get_random_quote(&self) -> Option<&Quote> {
        // TODO: exclude recent quotes
        let quote_index = thread_rng().gen_range(0, self.id_to_quote.len());
        self.id_to_quote.iter().nth(quote_index).map(|q| q.1)
    }

    pub fn count_quote(&self, query: Regex) -> usize {
        self.id_to_quote
            .values()
            .filter(|quote| query.is_match(&quote.content))
            .count()
    }

    pub fn len(&self) -> usize {
        self.id_to_quote.len()
    }

    pub fn search_quote(&mut self, query: Regex) -> Option<&Quote> {
        let matches: Vec<&Quote> = self
            .id_to_quote
            .values()
            .filter(|quote| query.is_match(&quote.content))
            .collect();

        if matches.is_empty() {
            return None;
        }

        for _ in 0..matches.len() {
            let quote_index = thread_rng().gen_range(0, matches.len());
            let quote = matches[quote_index];

            if self.quote_memory.add(quote.id) {
                return Some(quote);
            }
        }

        // All quotes were recent, just pick a random one.
        let quote_index = thread_rng().gen_range(0, matches.len());
        let quote = matches[quote_index];
        self.quote_memory.add(quote.id);
        Some(quote)
    }

    pub fn add_quote(
        &mut self,
        submitter: String,
        content: String,
    ) -> Result<usize, failure::Error> {
        self.max_quote_id += 1;

        let quote = Quote {
            id: self.max_quote_id,
            date: Utc::now(),
            submitter,
            content,
            edit_history: vec![],
            edited_by: None,
            edited_at: None,
        };

        let json = serde_json::to_string(&quote)?;
        writeln!(&self.file, "{}", json)?;
        self.file.sync_data()?;
        self.id_to_quote.insert(self.max_quote_id, quote);

        Ok(self.max_quote_id)
    }

    pub fn edit_quote(
        &mut self,
        id: usize,
        editor: String,
        content: String,
    ) -> Result<Option<&Quote>, failure::Error> {
        let edited = if let Some(quote) = self.id_to_quote.get_mut(&id) {
            quote.edit_history.push(EditHistory {
                date: quote.date,
                content: quote.content.clone(),
            });

            quote.content = content;
            quote.edited_by = Some(editor);
            quote.edited_at = Some(Utc::now());

            true
        } else {
            false
        };

        if edited {
            self.rewrite_storage()?;
        }

        Ok(self.id_to_quote.get(&id))
    }

    pub fn rewrite_storage(&mut self) -> Result<(), std::io::Error> {
        let mut path = PathBuf::from(&self.file_path);
        path.set_extension("json.tmp");

        let mut file = OpenOptions::new()
            .read(true)
            .write(true)
            .create(true)
            .open(&path)?;

        let mut keys: Vec<usize> = self.id_to_quote.keys().cloned().collect();
        keys.sort();
        for key in keys {
            let quote = self.id_to_quote.get(&key).unwrap();
            let json = serde_json::to_string(quote)?;
            writeln!(&mut file, "{}", json)?;
        }

        std::fs::rename(path, &self.file_path)?;
        self.file = file;

        Ok(())
    }
}

impl TypeMapKey for QuoteStorage {
    type Value = QuoteStorage;
}

struct RecentQuoteMemory {
    length: usize,
    recent_quote_ids: VecDeque<usize>,
}

impl RecentQuoteMemory {
    pub fn new(length: usize) -> RecentQuoteMemory {
        RecentQuoteMemory {
            length,
            recent_quote_ids: VecDeque::new(),
        }
    }

    pub fn add(&mut self, quote_id: usize) -> bool {
        if self.recent_quote_ids.contains(&quote_id) {
            return false;
        }

        if self.recent_quote_ids.len() == self.length {
            self.recent_quote_ids.pop_back();
        }

        self.recent_quote_ids.push_front(quote_id);
        true
    }
}
