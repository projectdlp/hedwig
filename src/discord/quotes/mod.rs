use log::error;
use regex::Regex;
use regex::RegexBuilder;
use serenity::{
    framework::standard::{
        macros::{command, group},
        Args, CommandResult,
    },
    model::prelude::*,
    prelude::*,
};

use crate::discord::utils::formatted_message_name;

mod storage;

pub use storage::{Quote, QuoteStorage};

#[group]
#[commands(
    getquote,
    whoquote,
    countquote,
    searchquote,
    addquote,
    delquote,
    editquote
)]
struct Quotes;

#[command]
async fn getquote(context: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let data = context.data.read().await;
    let quote_storage = data
        .get::<QuoteStorage>()
        .expect("Expected QuoteStorage to be in ShareMap.");

    let quote = if args.is_empty() {
        quote_storage.get_random_quote()
    } else {
        let quote_id = args.single::<usize>()?;
        quote_storage.get_quote(quote_id)
    };

    if let Some(quote) = quote {
        msg.channel_id
            .say(
                &context.http,
                format!("[{}] {}", quote.id, quote.escaped_content()),
            )
            .await?;
    } else {
        msg.channel_id
            .say(&context.http, "Quote not found!")
            .await?;
    }

    Ok(())
}

#[command]
async fn whoquote(context: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let data = context.data.read().await;
    let quote_storage = data
        .get::<QuoteStorage>()
        .expect("Expected QuoteStorage to be in ShareMap.");

    let quote_id = args.single::<usize>()?;
    if let Some(quote) = quote_storage.get_quote(quote_id) {
        let mut whoquote = format!(
            "Quote {} added by {} on {}",
            quote.id,
            quote.submitter,
            quote.added_on_formatted()
        );

        if let (Some(edited_by), Some(edited_at)) = (&quote.edited_by, &quote.edited_on_formatted())
        {
            whoquote += format!(", edited by {} on {}", edited_by, edited_at).as_str();
        }

        msg.channel_id.say(&context.http, whoquote).await?;
    } else {
        msg.channel_id
            .say(&context.http, "Quote not found!")
            .await?;
    }

    Ok(())
}

#[command]
async fn countquote(context: &Context, msg: &Message, args: Args) -> CommandResult {
    let mut data = context.data.write().await;
    let quote_storage = data
        .get_mut::<QuoteStorage>()
        .expect("Expected QuoteStorage to be in ShareMap.");

    if args.is_empty() {
        msg.channel_id
            .say(
                &context.http,
                format!("{} total quotes", quote_storage.len()),
            )
            .await?;
    } else {
        let quote_re = args.rest();

        msg.channel_id
            .say(
                &context.http,
                format!(
                    "{} total quotes matching {}",
                    quote_storage.count_quote(build_query(quote_re)?),
                    quote_re
                ),
            )
            .await?;
    }

    Ok(())
}

#[command]
async fn searchquote(context: &Context, msg: &Message, args: Args) -> CommandResult {
    let mut data = context.data.write().await;
    let quote_storage = data
        .get_mut::<QuoteStorage>()
        .expect("Expected QuoteStorage to be in ShareMap.");

    let quote_re = args.rest();

    if let Some(quote) = quote_storage.search_quote(build_query(quote_re)?) {
        msg.channel_id
            .say(
                &context.http,
                format!("[{}] {}", quote.id, quote.escaped_content()),
            )
            .await?;
    } else {
        msg.channel_id
            .say(&context.http, "Quote not found!")
            .await?;
    }

    Ok(())
}

#[command]
async fn addquote(context: &Context, msg: &Message, args: Args) -> CommandResult {
    let submitter = formatted_message_name(context, msg).await;
    let mut data = context.data.write().await;
    let quote_storage = data
        .get_mut::<QuoteStorage>()
        .expect("Expected QuoteStorage to be in ShareMap.");

    let content = args.rest();
    if content.trim().is_empty() {
        return Ok(());
    }

    let x = quote_storage.add_quote(submitter, content.to_string());
    if let Ok(quote_id) = x {
        msg.channel_id
            .say(&context.http, format!("Added quote [{}]", quote_id))
            .await?;
    } else {
        error!("Error: {:?}", x);
        msg.channel_id
            .say(&context.http, "Unable to add quote :(")
            .await?;
    }

    Ok(())
}

#[command]
#[allowed_roles("Moderator", "Admin")]
async fn editquote(context: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let editor = formatted_message_name(context, msg).await;
    let mut data = context.data.write().await;
    let quote_storage = data
        .get_mut::<QuoteStorage>()
        .expect("Expected QuoteStorage to be in ShareMap.");

    if args.is_empty() {
        msg.channel_id
            .say(&context.http, "Must provide a quote id to edit")
            .await?;
    } else {
        let quote_id = args.single::<usize>()?;
        let content = args.rest();
        if content.trim().is_empty() {
            return Ok(());
        }

        match quote_storage.edit_quote(quote_id, editor, content.to_string())? {
            Some(q) => {
                msg.channel_id
                    .say(
                        &context.http,
                        format!("Quote [{}] by {} edited.", q.id, q.submitter),
                    )
                    .await?
            }
            None => {
                msg.channel_id
                    .say(&context.http, format!("Quote not found [{}]", quote_id))
                    .await?
            }
        };
    }

    Ok(())
}

#[command]
#[allowed_roles("Moderator", "Admin")]
async fn delquote(context: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let mut data = context.data.write().await;
    let quote_storage = data
        .get_mut::<QuoteStorage>()
        .expect("Expected QuoteStorage to be in ShareMap.");

    if args.is_empty() {
        msg.channel_id
            .say(&context.http, "Must provide a quote id to remove")
            .await?;
    } else {
        let quote_id = args.single::<usize>()?;
        match quote_storage.del_quote(quote_id)? {
            Some(_) => msg.channel_id.say(&context.http, "Quote deleted.").await?,
            None => {
                msg.channel_id
                    .say(&context.http, format!("Quote not found [{}]", quote_id))
                    .await?
            }
        };
    }

    Ok(())
}

fn build_query(query: &str) -> Result<Regex, regex::Error> {
    RegexBuilder::new(query).case_insensitive(true).build()
}
