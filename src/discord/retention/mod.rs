use crate::common::Result;
use crate::config::HedwigConfig;
use crate::discord::channel::HedwigMessagesIter;
use crate::discord::storage::message::StoredMessage;

use chrono::{DateTime, Utc};
use itertools::Itertools;
use log::{info, warn};
use serenity::client::Context;
use serenity::futures::StreamExt;
use serenity::http::Http;
use serenity::model::id::{ChannelId, MessageId};
use sqlx::PgPool;
use std::collections::HashSet;

pub async fn run_periodic_work(ctx: Context, config: HedwigConfig, dbpool: PgPool) {
    if let Some(polling_period) = config.retention().polling_period() {
        info!("Starting retention polling, every {}", polling_period);
        let polling_period =
            tokio::time::Duration::from_millis(polling_period.num_milliseconds() as u64);
        let mut interval_day = tokio::time::interval(polling_period);
        loop {
            if let Err(e) = apply_retention_horizons(&ctx, &config, &dbpool).await {
                warn!("Failed to apply retention horizons: {:?}", e);
            }
            let _now = interval_day.tick().await;
        }
    } else {
        info!("Retention polling not configured. Exiting.");
    }
}
pub async fn apply_retention_horizons(
    ctx: &Context,
    config: &HedwigConfig,
    dbpool: &PgPool,
) -> Result<()> {
    info!("Starting retention scan...");
    let now = Utc::now();
    let retention = config.retention().channel_to_retention();

    for (channel_id, duration) in retention.into_iter() {
        let retention_horizon = now - duration;
        apply_retention_horizon(ctx, &dbpool, channel_id, retention_horizon).await?;
    }

    Ok(())
}

async fn apply_retention_horizon(
    ctx: &Context,
    dbpool: &PgPool,
    channel_id: ChannelId,
    retention_horizon: DateTime<Utc>,
) -> Result<()> {
    info!("Scanning {}", channel_id);
    // Lookup oldest message before the retention horizon.
    let newest_message_before_horizon =
        StoredMessage::newest_before_horizon(dbpool, &channel_id, retention_horizon).await?;

    info!("Oldest message: {:?}", newest_message_before_horizon);
    let mut messages = match newest_message_before_horizon {
        Some(message_id) => {
            HedwigMessagesIter::<Http>::stream_before(&ctx, channel_id, message_id).boxed()
        }
        None => HedwigMessagesIter::<Http>::stream(&ctx, channel_id).boxed(),
    };

    // Scan messages, adding all candidates to deletion set.
    let mut messages_to_delete: HashSet<MessageId> = HashSet::new();
    while let Some(message_result) = messages.next().await {
        let message = message_result?;

        if message.timestamp < retention_horizon && !message.pinned {
            messages_to_delete.insert(message.id);
        }

        if message.timestamp > retention_horizon {
            break;
        }
    }

    info!("Found {} messages to delete", messages_to_delete.len());

    // Execute deletions
    let chunks = {
        let chunks = &messages_to_delete.into_iter().chunks(100);
        chunks
            .into_iter()
            .map(|c| c.collect::<Vec<_>>())
            .collect::<Vec<_>>()
    };
    for chunk in chunks {
        channel_id.delete_messages(&ctx, chunk).await?;
    }

    // Clean index. (TODO)

    Ok(())
}
