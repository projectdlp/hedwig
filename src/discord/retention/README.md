# Retention Module

Implements a best-effort limited retention feature for channels.

- Scans the channel backwards from the oldest known message (in the stored message index) still within the retention horizon.
- Collects all message ids outside the horizon.
- Every X messages, flushes a mass deletion request to Discord.
- Stored message index updates to delete all messages older than the retention horizon.

This process runs at startup and every 15 minutes after that.