use crate::config::HedwigConfig;

use crate::discord::HedwigState;
use crate::discord::Result;
use chrono::Duration;
use failure::format_err;
use futures::stream::TryStreamExt;
use futures::StreamExt;

use log::{info, warn};

use crate::discord::moderation::temporary_role::RoleApplicationResult;
use humantime::FormattedDuration;
use serenity::builder::{CreateApplicationCommand, CreateApplicationCommandPermissionsData};
use serenity::{model::prelude::*, prelude::*};
use sqlx::{PgPool, Pool, Postgres};

mod temporary_role;

static AZKABAN_ROLE_ID: RoleId = RoleId(755591230053810180);
static ADMIN_ROLE_ID: RoleId = RoleId(602551634098389003);
static MODERATOR_ROLE_ID: RoleId = RoleId(307403215849127937);
static AZKABANNED_CHANNEL_ID: ChannelId = ChannelId(755593533917888662);

/// /jail <user> <duration>: send a user to jail
/// /jailme <duration-upto-7d>: sends the caller to Azkaban with a friendlier message
/// /unjail <user>: remove a user from jail
/// /role temp add <user> <role> <duration>: add a user to a role for a period of time
/// /role temp remove <user> <role>: remove a user from a role immediately
/// /role public add <role> <description>: makes a role available to be added by users
/// /role public remove <role> <description>: removes a role from the public registry, does not remove existing users from role
/// /roles: show public roles & allow addition.
pub async fn register_interactions(context: &Context, guild: GuildId) -> Result<()> {
    let mut jail = CreateApplicationCommand::default();
    jail.name("jail")
        .description("Sends a user to #azkabanned")
        .create_option(|o| {
            o.name("user")
                .required(true)
                .description("User to send to Azkaban")
                .kind(ApplicationCommandOptionType::User)
        })
        .create_option(|o| {
            o.name("duration")
                .required(true)
                .description("Duration to jail user, e.x. 4h, or 1d8h, or 1M")
                .kind(ApplicationCommandOptionType::String)
        })
        .default_permission(false);
    let mut unjail = CreateApplicationCommand::default();
    unjail
        .name("unjail")
        .description("Removes a user to #azkabanned early")
        .create_option(|o| {
            o.name("user")
                .required(true)
                .description("User to remove from Azkaban")
                .kind(ApplicationCommandOptionType::User)
        })
        .default_permission(false);

    let mut staff_commands = vec![jail, unjail];
    staff_commands.extend(temporary_role::create_commands(&context, guild).await?);

    let commands = guild
        .create_application_commands(&context, |f| f.set_application_commands(staff_commands))
        .await?;

    for command in commands {
        guild
            .create_application_command_permission(&context, command.id, apply_staff_permissions)
            .await?;
    }

    Ok(())
}

fn apply_staff_permissions(
    permissions: &mut CreateApplicationCommandPermissionsData,
) -> &mut CreateApplicationCommandPermissionsData {
    permissions.create_permission(|permission| {
        permission
            .id(ADMIN_ROLE_ID.0)
            .kind(ApplicationCommandPermissionType::Role)
            .permission(true)
    });
    permissions.create_permission(|permission| {
        permission
            .id(MODERATOR_ROLE_ID.0)
            .kind(ApplicationCommandPermissionType::Role)
            .permission(true)
    });
    permissions
}

pub async fn run_periodic_work(ctx: Context, config: HedwigConfig, dbpool: PgPool) {
    let polling_period = config.moderation().polling_period();
    info!("Starting moderation polling task, every {}", polling_period);
    let polling_period =
        tokio::time::Duration::from_millis(polling_period.num_milliseconds() as u64);
    let mut interval = tokio::time::interval(polling_period);
    loop {
        if let Err(e) = temporary_role::remove_expired_roles(&ctx, &config, &dbpool).await {
            warn!("Failed to remove roles: {:?}", e);
        }
        let _ = interval.tick().await;
    }
}

pub async fn jail_interaction(context: &Context, interaction: &Interaction) -> Result<()> {
    let dbpool = {
        let data = context.data.read().await;
        let state = data
            .get::<HedwigState>()
            .expect("Expected HedwigState to be in ShareMap.");
        state.dbpool.clone()
    };

    let actor = interaction
        .member
        .as_ref()
        .map(|m| m.user.clone())
        .ok_or_else(|| format_err!("Actor is missing"))?;

    let data = match interaction.data.as_ref().unwrap() {
        InteractionData::ApplicationCommand(data) => data,
        InteractionData::MessageComponent(_) => return Ok(()),
    };
    let guild_id = interaction
        .guild_id
        .ok_or_else(|| format_err!("Guild id missing"))?;

    let user = extract_user(&*data.options)?;
    let duration_str = extract_duration(&*data.options)?;

    let mut member = guild_id.member(&context, user).await?;
    let role_duration = Duration::from_std(humantime::parse_duration(&duration_str)?).unwrap();
    let human_jail_duration = humantime::format_duration(role_duration.to_std().unwrap());

    info!("Jailing {:?} for: {:?}", member, human_jail_duration);

    match temporary_role::apply_temporary_role(
        &context,
        &dbpool,
        &mut member,
        AZKABAN_ROLE_ID,
        &actor,
        role_duration,
    )
    .await?
    {
        RoleApplicationResult::AppliedNewRole => {
            send_welcome(&context, interaction, user, member, human_jail_duration).await?;
        }
        RoleApplicationResult::UpdatedExpiration { before, after } => {
            text_reply(
                &context,
                interaction,
                format!(
                    "Updated role expiration for: {}, time delta: {}, expires: {}",
                    member.display_name(),
                    humantime::format_duration((before - after).to_std()?),
                    after.to_rfc3339()
                ),
            )
            .await?
        }
    }

    Ok(())
}

async fn send_welcome(
    context: &&Context,
    interaction: &Interaction,
    user: &User,
    member: Member,
    human_jail_duration: FormattedDuration,
) -> Result<()> {
    // Send welcome message :)
    AZKABANNED_CHANNEL_ID
        .send_message(&context, |m| {
            m.content(format_args!(
                "{member} has joined the Azkabanned. They're locked in for {duration}, and \
            will be released automatically after that time.",
                member = user.mention(),
                duration = human_jail_duration
            ))
        })
        .await?;

    // Attempt to send welcome PM.
    let dm_result = user
        .direct_message(&context, |m| {
            m.content(format_args!(
            "Welcome to DLP's Azkaban, {member}. You're locked in #azkabanned for {duration}, and \
            will be released automatically after that time. Use this time to reflect on your \
            wrongs.",
            member = user.mention(),
            duration = human_jail_duration
        ))
        })
        .await;
    if let Err(err) = dm_result {
        warn!("Failed to send DM to {:?}: {:?}", user, err);
    }

    text_reply(
        &context,
        interaction,
        format!(
            "Jailing {} for: {}",
            member.display_name(),
            human_jail_duration
        ),
    )
    .await
}

pub async fn unjail_interaction(context: &Context, interaction: &Interaction) -> Result<()> {
    let dbpool = {
        let data = context.data.read().await;
        let state = data
            .get::<HedwigState>()
            .expect("Expected HedwigState to be in ShareMap.");
        state.dbpool.clone()
    };

    let data = match interaction.data.as_ref().unwrap() {
        InteractionData::ApplicationCommand(data) => data,
        InteractionData::MessageComponent(_) => return Ok(()),
    };
    let guild_id = interaction
        .guild_id
        .ok_or_else(|| format_err!("Guild id missing"))?;

    let user = extract_user(&*data.options)?;
    let mut member = guild_id.member(&context, user).await?;

    let message =
        if temporary_role::remove_role(context, &dbpool, &mut member, AZKABAN_ROLE_ID).await? {
            format!("Unjailed {:?}", member.display_name())
        } else {
            "User isn't in jail.".to_string()
        };

    text_reply(&context, interaction, message).await?;

    Ok(())
}

pub async fn role_interaction(context: &Context, interaction: &Interaction) -> Result<()> {
    let dbpool = {
        let data = context.data.read().await;
        let state = data
            .get::<HedwigState>()
            .expect("Expected HedwigState to be in ShareMap.");
        state.dbpool.clone()
    };

    let actor = interaction
        .member
        .as_ref()
        .map(|m| m.user.clone())
        .ok_or_else(|| format_err!("Actor is missing"))?;

    let guild_id = interaction
        .guild_id
        .ok_or_else(|| format_err!("Guild id missing"))?;

    let data = match interaction.data.as_ref().unwrap() {
        InteractionData::ApplicationCommand(data) => data,
        InteractionData::MessageComponent(_) => return Ok(()),
    };

    println!("{:?}", data);

    for option in &data.options {
        match option.name.as_str() {
            "temp" => {
                for sub_option in &option.options {
                    match sub_option.name.as_str() {
                        "add" => {
                            temporary_role::role_add_interaction(
                                &context, &dbpool, &actor, guild_id, data, sub_option,
                            )
                            .await?;
                        }
                        "remove" => {}
                        cmd => warn!("Unhandled subcommand: {}", cmd),
                    }
                }
            }
            cmd => warn!("Unhandled command: {}", cmd),
        }
    }

    Ok(())
}

async fn text_reply(context: &&Context, interaction: &Interaction, message: String) -> Result<()> {
    interaction
        .create_interaction_response(&context, |r| {
            r.kind(InteractionResponseType::ChannelMessageWithSource)
                .interaction_response_data(|d| d.content(message))
        })
        .await?;

    Ok(())
}

fn extract_duration(options: &[ApplicationCommandInteractionDataOption]) -> Result<&str> {
    extract_data_option(options, "duration", |o| match o {
        ApplicationCommandInteractionDataOptionValue::String(str) => Some(str.as_str()),
        _ => None,
    })
    .ok_or_else(|| format_err!("Duration missing"))
}

fn extract_user(options: &[ApplicationCommandInteractionDataOption]) -> Result<&User> {
    extract_data_option(options, "user", |o| match o {
        ApplicationCommandInteractionDataOptionValue::User(user, _) => Some(user),
        _ => None,
    })
    .ok_or_else(|| format_err!("User missing"))
}

fn extract_role(options: &[ApplicationCommandInteractionDataOption]) -> Result<&Role> {
    extract_data_option(options, "role_id", |o| match o {
        ApplicationCommandInteractionDataOptionValue::Role(role) => Some(role),
        _ => None,
    })
    .ok_or_else(|| format_err!("Role missing"))
}

fn extract_data_option<
    'a,
    U,
    F: FnOnce(&'a ApplicationCommandInteractionDataOptionValue) -> Option<U>,
>(
    options: &'a [ApplicationCommandInteractionDataOption],
    name: &'a str,
    data_fn: F,
) -> Option<U> {
    options
        .iter()
        .find(|o| o.name == name)
        .and_then(|o| o.resolved.as_ref())
        .and_then(data_fn)
}
