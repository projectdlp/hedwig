use crate::common::Result;
use crate::config::HedwigConfig;
use crate::discord::storage::temporary_role;
use crate::discord::storage::temporary_role::TemporaryRole;
use crate::discord::utils::formatted_name;
use chrono::{DateTime, Duration, Utc};
use futures::stream::{self, TryStreamExt};
use futures::StreamExt;
use log::info;

use crate::discord::moderation::{
    apply_staff_permissions, extract_duration, extract_role, extract_user,
};
use serenity::builder::CreateApplicationCommand;
use serenity::client::Context;
use serenity::model::guild::Member;
use serenity::model::id::{GuildId, MessageId, RoleId, UserId};
use serenity::model::interactions::{
    ApplicationCommand, ApplicationCommandInteractionData, ApplicationCommandInteractionDataOption,
    ApplicationCommandOptionType,
};
use serenity::model::user::User;
use sqlx::PgPool;

pub async fn create_commands(
    context: &Context,
    guild: GuildId,
) -> Result<Vec<CreateApplicationCommand>> {
    let mut commands = CreateApplicationCommand::default();
    commands
        .name("role")
        .description("Commands to manage roles")
        .create_option(|o| {
            o.name("temp")
                .description("Allows applying & removing temporary roles")
                .kind(ApplicationCommandOptionType::SubCommandGroup)
                .create_sub_option(|temp| {
                    temp.name("add")
                        .kind(ApplicationCommandOptionType::SubCommand)
                        .description("Applies a temporary role")
                        .create_sub_option(|o| {
                            o.name("user")
                                .required(true)
                                .description("User to apply role to.")
                                .kind(ApplicationCommandOptionType::User)
                        })
                        .create_sub_option(|o| {
                            o.name("role")
                                .required(true)
                                .description("Role to apply to user.")
                                .kind(ApplicationCommandOptionType::Role)
                        })
                        .create_sub_option(|o| {
                            o.name("duration")
                                .required(true)
                                .description("Duration to jail user, e.x. 4h, or 1d8h, or 1M")
                                .kind(ApplicationCommandOptionType::String)
                        })
                })
                .create_sub_option(|temp| {
                    temp.name("remove")
                        .description("Removes a temporary role")
                        .kind(ApplicationCommandOptionType::SubCommand)
                        .create_sub_option(|o| {
                            o.name("user")
                                .required(true)
                                .description("User to apply role to.")
                                .kind(ApplicationCommandOptionType::User)
                        })
                        .create_sub_option(|o| {
                            o.name("duration")
                                .required(true)
                                .description("Duration to jail user, e.x. 4h, or 1d8h, or 1M")
                                .kind(ApplicationCommandOptionType::String)
                        })
                })
        })
        .default_permission(false);

    Ok(vec![commands])
}

pub async fn remove_expired_roles(
    context: &Context,
    _config: &HedwigConfig,
    dbpool: &PgPool,
) -> Result<()> {
    info!("Starting expired role removal task");
    let horizon = Utc::now();
    let removed_roles: Result<Vec<(String, RoleId)>> =
        stream::iter(TemporaryRole::list_expired_roles(dbpool, horizon).await?)
            .then(|expired_role| async move {
                let user_id = expired_role.user_id();
                let guild_id = expired_role.guild_id();
                let mut member = guild_id.member(&context, user_id).await?;
                remove_role(context, dbpool, &mut member, expired_role.role_id()).await?;

                let name = formatted_name(&context, guild_id, &member.user).await;
                Ok((name, expired_role.role_id()))
            })
            .try_collect()
            .await;
    let roles_removed = removed_roles?;

    if !roles_removed.is_empty() {
        info!("Cleaned up expired roles: {:?}", roles_removed);
    }

    Ok(())
}

pub async fn role_add_interaction(
    context: &&Context,
    dbpool: &PgPool,
    actor: &User,
    guild_id: GuildId,
    data: &ApplicationCommandInteractionData,
    sub_option: &ApplicationCommandInteractionDataOption,
) {
    let user = extract_user(&*sub_option.options)?;
    let mut member = guild_id.member(&context, user).await?;
    let duration_str = extract_duration(&*data.options)?;
    let role = extract_role(&*data.options)?;
    let role_duration = Duration::from_std(humantime::parse_duration(&duration_str)?).unwrap();
    let human_jail_duration = humantime::format_duration(role_duration.to_std().unwrap());
    temporary_role::apply_temporary_role(
        context,
        &dbpool,
        &mut member,
        role.id,
        &actor,
        role_duration,
    )
    .await?;
}

pub async fn role_remove_interaction(
    context: &&Context,
    dbpool: &PgPool,
    actor: &User,
    guild_id: GuildId,
    data: &ApplicationCommandInteractionData,
    sub_option: &ApplicationCommandInteractionDataOption,
) {
    let user = extract_user(&*sub_option.options)?;
    let mut member = guild_id.member(&context, user).await?;
    let duration_str = extract_duration(&*data.options)?;
    let role = extract_role(&*data.options)?;
    let role_duration = Duration::from_std(humantime::parse_duration(&duration_str)?).unwrap();
    let human_jail_duration = humantime::format_duration(role_duration.to_std().unwrap());
    temporary_role::re(
        context,
        &dbpool,
        &mut member,
        role.id,
        &actor,
        role_duration,
    )
    .await?;
}

pub enum RoleApplicationResult {
    AppliedNewRole,
    UpdatedExpiration {
        before: DateTime<Utc>,
        after: DateTime<Utc>,
    },
}

pub async fn apply_temporary_role(
    context: &Context,
    dbpool: &PgPool,
    user: &mut Member,
    role: RoleId,
    actor: &User,
    role_duration: Duration,
) -> Result<RoleApplicationResult> {
    // TODO: update if already exists
    let remove_at = Utc::now() + role_duration;
    if let Some(temporary_role) =
        TemporaryRole::role_by_user(dbpool, user.user.id, user.guild_id, role).await?
    {
        TemporaryRole::update(dbpool, &user, role, actor, &remove_at).await?;

        return Ok(RoleApplicationResult::UpdatedExpiration {
            before: temporary_role.remove_at,
            after: remove_at,
        });
    };

    TemporaryRole::insert(dbpool, user, role, actor, &remove_at).await?;

    // Add role
    user.add_role(&context, role).await?;

    Ok(RoleApplicationResult::AppliedNewRole)
}

pub async fn update_role_expiration(_ban: &TemporaryRole) -> Result<()> {
    // TODO
    Ok(())
}

pub async fn remove_role(
    context: &Context,
    dbpool: &PgPool,
    user: &mut Member,
    role: RoleId,
) -> Result<bool> {
    match TemporaryRole::role_by_user(dbpool, user.user.id, user.guild_id, role).await? {
        Some(temporary_role) => {
            let mut user = temporary_role
                .guild_id()
                .member(&context, temporary_role.user_id())
                .await?;

            // Remove role
            user.remove_role(context, role).await?;

            // Remove database row
            TemporaryRole::remove(
                dbpool,
                user.user.id,
                user.guild_id,
                temporary_role.role_id(),
            )
            .await?;

            Ok(true)
        }
        None => Ok(false),
    }
}
