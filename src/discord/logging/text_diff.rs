use difference::{Changeset, Difference};
use std::fmt::Write;

pub(crate) struct TextDiff {
    changeset: Changeset,
}

impl TextDiff {
    pub(crate) fn diff(text1: &str, text2: &str) -> Option<TextDiff> {
        let diff = TextDiff {
            changeset: Changeset::new(text1, text2, ""),
        };

        if diff.has_diff() {
            Some(diff)
        } else {
            None
        }
    }

    pub(crate) fn has_diff(&self) -> bool {
        self.changeset.distance > 0
    }

    #[allow(unused_must_use)] // write! is writing to a String.
    #[allow(clippy::unnecessary_filter_map)]
    pub(crate) fn to_markdown_diff(&self) -> String {
        let mut output = String::new();
        let Changeset { ref diffs, .. } = self.changeset;

        let additions = diffs
            .iter()
            .filter_map(|d| match d {
                Difference::Add(_) => Some(d),
                _ => None,
            })
            .collect::<Vec<_>>();
        let removals = diffs
            .iter()
            .filter_map(|d| match d {
                Difference::Rem(_) => Some(d),
                _ => None,
            })
            .collect::<Vec<_>>();

        if !removals.is_empty() {
            write!(output, "`-` ");
            let mut strike_open = false;
            for c in diffs {
                match *c {
                    Difference::Same(ref z) => {
                        if strike_open {
                            strike_open = false;
                            write!(output, "~~");
                        }
                        write!(output, "{}", z);
                    }
                    Difference::Rem(ref z) => {
                        if !strike_open {
                            strike_open = true;
                            write!(output, "~~");
                        }
                        write!(output, "{}", z);
                    }
                    _ => (),
                }
            }
            if strike_open {
                write!(output, "~~");
            }
        }

        if !additions.is_empty() {
            if !removals.is_empty() {
                writeln!(output);
            }

            write!(output, "`+` ");
            let mut bold_open = false;
            for c in diffs {
                match *c {
                    Difference::Same(ref z) => {
                        if bold_open {
                            bold_open = false;
                            write!(output, "**");
                        }
                        write!(output, "{}", z);
                    }
                    Difference::Add(ref z) => {
                        if !bold_open {
                            bold_open = true;
                            write!(output, "**");
                        }
                        write!(output, "{}", z);
                    }
                    _ => (),
                }
            }
            if bold_open {
                write!(output, "**");
            }
        }

        output
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn diff_no_diff() {
        let diff = TextDiff::diff("Foo", "Foo");

        assert_eq!(diff.is_some(), false);
    }

    #[test]
    fn diff_add_only() {
        let diff = TextDiff::diff("Foo", "FooBar").unwrap();

        assert_eq!(diff.has_diff(), true);
        assert_eq!(diff.to_markdown_diff(), "`+` Foo**Bar**");
    }

    #[test]
    fn diff_remove_only() {
        let diff = TextDiff::diff("FooBar", "Foo").unwrap();

        assert_eq!(diff.has_diff(), true);
        assert_eq!(diff.to_markdown_diff(), "`-` Foo~~Bar~~");
    }

    #[test]
    fn diff_add_remove() {
        let diff = TextDiff::diff("FooBar", "BarBaz").unwrap();

        assert_eq!(diff.has_diff(), true);
        assert_eq!(
            diff.to_markdown_diff(),
            "`-` ~~Foo~~Bar\n\
             `+` Bar**Baz**"
        );
    }
}
