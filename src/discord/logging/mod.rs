use failure::err_msg;

use log::info;
use serenity::model::channel::{ChannelType, GuildChannel};
use serenity::model::guild::GuildInfo;
use serenity::model::{
    channel::Channel,
    event::MessageUpdateEvent,
    id::{ChannelId, MessageId},
};
use serenity::utils::Colour;
use serenity::{model::channel::Message, prelude::*};
use std::collections::HashMap;

use crate::config::HedwigConfig;
use crate::discord::logging::text_diff::TextDiff;
use crate::discord::storage::message::StoredMessage;
use crate::discord::{utils, HedwigState};
use chrono::Utc;
use time::Duration;

mod text_diff;

pub(crate) struct LoggingHandler {
    config: HedwigConfig,
}

impl LoggingHandler {
    pub(crate) fn new(config: HedwigConfig) -> LoggingHandler {
        LoggingHandler { config }
    }

    pub(crate) async fn load_history(&self, ctx: &Context) -> Result<(), failure::Error> {
        let data = ctx.data.read().await;
        let state: &HedwigState = data.get::<HedwigState>().unwrap();

        if state.config.retention().skip_resync() {
            info!("Skipping resync");
            return Ok(());
        }

        let user = ctx.http.get_current_user().await?;
        let guilds: Vec<GuildInfo> = user.guilds(&ctx).await?;
        info!("Found {} guilds to process", guilds.len());

        for guild in guilds {
            let channels: HashMap<ChannelId, GuildChannel> = guild.id.channels(&ctx).await?;
            info!(
                "Found {} channels to process for guild {} ({})",
                channels.len(),
                guild.name,
                guild.id
            );
            for channel in channels.values() {
                if channel.kind != ChannelType::Text
                    || !channel
                        .permissions_for_user(&ctx, &user)
                        .await?
                        .read_message_history()
                {
                    info!("Skipping history for #{} ({})", channel.name, channel.id);
                    continue;
                }

                self.load_history_for_channel(&ctx, &state, &guild, &channel)
                    .await?;
            }
        }

        Ok(())
    }

    async fn load_history_for_channel(
        &self,
        ctx: &Context,
        state: &HedwigState,
        guild: &GuildInfo,
        channel: &GuildChannel,
    ) -> Result<(), failure::Error> {
        const MESSAGES_PER_PAGE: usize = 100;

        // Message horizon is how far we'll go back in time to reload scrollback. As of now, that's
        // 24 hours.
        let message_horizon = Utc::now() - Duration::days(1);

        info!("Loading history for #{} ({})", channel.name, channel.id);

        if let Some(mut latest_stored_id) =
            StoredMessage::latest_message_id(&state.dbpool, &channel.id)
                .await
                .unwrap_or(None)
        {
            // Page until we're back at real time.
            loop {
                let mut messages: Vec<Message> = channel
                    .messages(&ctx, |m| {
                        m.after(latest_stored_id).limit(MESSAGES_PER_PAGE as u64)
                    })
                    .await?;

                info!(
                    "Loaded {} messages after stored={}",
                    messages.len(),
                    latest_stored_id
                );

                if messages.is_empty() {
                    break;
                }

                let (latest_message_id, latest_timestamp) = {
                    let msg = messages.iter().max_by_key(|&m| m.id.0).unwrap();

                    (msg.id.clone(), msg.timestamp)
                };

                if latest_timestamp < message_horizon {
                    info!("Latest message in page is before horizon, skipping reload.");
                    break;
                }

                for message in messages.iter_mut() {
                    message.guild_id = Some(guild.id);
                    StoredMessage::insert(&state.dbpool, &ctx, message).await?;
                }

                if messages.len() < MESSAGES_PER_PAGE {
                    break;
                } else {
                    latest_stored_id = latest_message_id
                }
            }
        } else {
            // Page back to 100 messages.
            let mut messages: Vec<Message> = channel.messages(&ctx, |m| m.limit(100)).await?;

            info!("Loaded latest {} messages", messages.len());

            for message in messages.iter_mut() {
                message.guild_id = Some(guild.id);
                StoredMessage::insert(&state.dbpool, &ctx, message).await?;
            }
        }

        info!("Done loading messages.");

        Ok(())
    }

    pub(crate) async fn message_created(
        &self,
        ctx: &Context,
        message: &Message,
    ) -> Result<(), failure::Error> {
        let data = ctx.data.read().await;
        let state: &HedwigState = data.get::<HedwigState>().unwrap();

        Ok(StoredMessage::insert(&state.dbpool, ctx, message).await?)
    }

    pub(crate) async fn message_updated(
        &self,
        ctx: Context,
        event: &MessageUpdateEvent,
    ) -> Result<(), failure::Error> {
        let data = ctx.data.read().await;
        let state: &HedwigState = data.get::<HedwigState>().unwrap();
        let config = &state.config;

        if self.config.logging().is_none() {
            return Ok(());
        }

        if event.guild_id.is_none() {
            return Err(err_msg("Message update missing guild or author"));
        }

        let new_content = event.content.as_ref().ok_or_else(|| {
            err_msg("Message update doesn't have any content, likely embed update.")
        })?;

        let old_message = match StoredMessage::fetch(&state.dbpool, &event.id).await? {
            Some(message) => message,
            None => return Err(err_msg("Couldn't find stored message")),
        };

        // Apply edits to database state.
        old_message.edit(&state.dbpool, &event).await?;

        // Don't log edits in retention-limited channels.
        if config
            .retention()
            .channel_has_retention_limit(&event.channel_id)
        {
            return Ok(());
        }

        let editor = event
            .author
            .as_ref()
            .ok_or_else(|| err_msg("missing editor, skipping log."))?;

        if editor.bot {
            return Err(err_msg(format!("Ignoring bot user {}", editor.tag())));
        }

        let editor_name =
            utils::formatted_name(&ctx, event.guild_id.as_ref().unwrap(), editor).await;

        let channel = match event.channel_id.to_channel(&ctx).await? {
            Channel::Guild(guild_channel) => guild_channel,
            _ => return Err(err_msg("Not a guild channel")),
        };

        let text_diff =
            TextDiff::diff(&old_message.content, new_content).ok_or_else(|| err_msg("No diff"))?;

        self.config
            .logging()
            .unwrap()
            .log_channel()
            .send_message(&ctx, |create_message| {
                create_message.embed(|embed| {
                    embed
                        .author(|author| author.name(editor_name).icon_url(editor.face()))
                        .title(format!("Message edited in #{}", channel.name()))
                        .color(Colour::GOLD)
                        .description(text_diff.to_markdown_diff());

                    if let Some(timestamp) = event.edited_timestamp {
                        embed.timestamp(&timestamp);
                    }

                    embed.url(format!(
                        "https://discordapp.com/channels/{}/{}/{}",
                        old_message.guild_id, old_message.channel_id, old_message.message_id
                    ));

                    embed
                })
            })
            .await?;

        Ok(())
    }

    pub(crate) async fn message_deleted(
        &self,
        ctx: Context,
        channel_id: ChannelId,
        message_id: MessageId,
    ) -> Result<(), failure::Error> {
        let data = ctx.data.read().await;
        let state: &HedwigState = data.get::<HedwigState>().unwrap();
        let config = &state.config;

        if self.config.logging().is_none() {
            return Ok(());
        }

        let channel = match channel_id.to_channel(&ctx).await? {
            Channel::Guild(guild_channel) => guild_channel,
            _ => return Ok(()),
        };

        // Don't log deletes in retention-limited channels.
        if config.retention().channel_has_retention_limit(&channel_id) {
            return Ok(());
        }

        if let Some(ref message) = StoredMessage::fetch(&state.dbpool, &message_id).await? {
            self.config
                .logging()
                .unwrap()
                .log_channel()
                .send_message(&ctx, |create_message| {
                    create_message.embed(|embed| {
                        embed
                            .title(format!("Message deleted in #{}", channel.name()))
                            .color(Colour::RED)
                            .description(&message.content);

                        embed
                    })
                })
                .await?;
            message.soft_delete(&state.dbpool).await?;
        }

        Ok(())
    }
}
