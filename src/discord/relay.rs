use crate::common::{Event, MessagePart, RelayMessage};
use chrono::{DateTime, Utc};
use regex::{CaptureMatches, Captures, Regex};
use serenity::http::CacheHttp;
use serenity::model::id::GuildId;
use serenity::model::user::User;
use serenity::utils::ContentSafeOptions;

pub async fn to_relay_message(
    context: impl CacheHttp,
    author: &User,
    guild: &Option<GuildId>,
    message: &str,
    timestamp: &DateTime<Utc>,
) -> Option<RelayMessage> {
    let mut opts = ContentSafeOptions::new()
        .clean_everyone(false)
        .clean_here(false)
        .show_discriminator(false);

    let username = if let Some(ref guild_id) = guild {
        opts = opts.display_as_member_from(guild_id);

        author
            .nick_in(&context, guild_id)
            .await
            .unwrap_or_else(|| author.name.clone())
    } else {
        author.name.clone()
    };

    let content = serenity::utils::content_safe(&context.cache().unwrap(), &message, &opts).await;

    let event = Event::STRUCTURED_MESSAGE {
        message_parts: parse_structured_text(&content),
        raw_message: content,
    };
    Some(RelayMessage {
        username,
        event,
        date: timestamp.clone(),
    })
}

fn parse_structured_text(content: &str) -> Vec<MessagePart> {
    let re = Regex::new(r"<(a)?:([^:]+):(\d+)>").unwrap();
    let splitter = SplitCaptures::new(&re, content);
    let mut parts = Vec::new();
    for state in splitter {
        match state {
            SplitState::Unmatched(t) => {
                if !t.is_empty() {
                    parts.push(MessagePart::String {
                        message: t.to_string(),
                    });
                }
            }
            SplitState::Captured(caps) => {
                let part = match parse_emoji(&caps) {
                    Some(emoji) => MessagePart::Emoji {
                        id: emoji.id,
                        name: emoji.name.clone(),
                        url: emoji.url(),
                    },
                    None => MessagePart::String {
                        message: caps[0].to_string(),
                    },
                };
                parts.push(part);
            }
        }
    }
    parts
}

fn parse_emoji(capture: &Captures) -> Option<EmojiId> {
    let id = capture.get(3)?.as_str().parse::<u64>().ok()?;
    let name = capture.get(2)?.as_str().to_string();
    let animated = capture.get(1).is_some();

    Some(EmojiId { id, name, animated })
}

struct EmojiId {
    id: u64,
    name: String,
    animated: bool,
}

impl EmojiId {
    fn url(&self) -> String {
        let mut url = String::new();
        url.push_str("https://cdn.discordapp.com/emojis/");
        url.push_str(&self.id.to_string());
        if self.animated {
            url.push_str(".gif")
        } else {
            url.push_str(".png")
        }
        url
    }
}

struct SplitCaptures<'r, 't> {
    finder: CaptureMatches<'r, 't>,
    text: &'t str,
    last: usize,
    caps: Option<Captures<'t>>,
}

impl<'r, 't> SplitCaptures<'r, 't> {
    fn new(re: &'r Regex, text: &'t str) -> SplitCaptures<'r, 't> {
        SplitCaptures {
            finder: re.captures_iter(text),
            text,
            last: 0,
            caps: None,
        }
    }
}

#[derive(Debug)]
enum SplitState<'t> {
    Unmatched(&'t str),
    Captured(Captures<'t>),
}

impl<'r, 't> Iterator for SplitCaptures<'r, 't> {
    type Item = SplitState<'t>;

    fn next(&mut self) -> Option<SplitState<'t>> {
        if let Some(caps) = self.caps.take() {
            return Some(SplitState::Captured(caps));
        }
        match self.finder.next() {
            None => {
                if self.last >= self.text.len() {
                    None
                } else {
                    let s = &self.text[self.last..];
                    self.last = self.text.len();
                    Some(SplitState::Unmatched(s))
                }
            }
            Some(caps) => {
                let m = caps.get(0).unwrap();
                let unmatched = &self.text[self.last..m.start()];
                self.last = m.end();
                self.caps = Some(caps);
                Some(SplitState::Unmatched(unmatched))
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_structured_text() {
        assert_eq!(
            parse_structured_text(
                "Hey, <a:partyparrot:644125122466283520>, <:smug:123> how's it going?"
            ),
            vec![
                MessagePart::String {
                    message: "Hey, ".to_string()
                },
                MessagePart::Emoji {
                    id: 644125122466283520,
                    name: "partyparrot".to_string(),
                    url: "https://cdn.discordapp.com/emojis/644125122466283520.gif".to_string()
                },
                MessagePart::String {
                    message: ", ".to_string()
                },
                MessagePart::Emoji {
                    id: 123,
                    name: "smug".to_string(),
                    url: "https://cdn.discordapp.com/emojis/123.png".to_string()
                },
                MessagePart::String {
                    message: " how's it going?".to_string()
                },
            ]
        )
    }

    #[test]
    fn test_parse_structured_text_single_emoji() {
        assert_eq!(
            parse_structured_text("<a:partyparrot:644125122466283520>"),
            vec![MessagePart::Emoji {
                id: 644125122466283520,
                name: "partyparrot".to_string(),
                url: "https://cdn.discordapp.com/emojis/644125122466283520.gif".to_string()
            },]
        )
    }
}
