use chrono::Duration;
use serde::Deserialize;
use serenity::model::id::ChannelId;
use std::collections::HashMap;

#[derive(Default, Debug, Clone, Deserialize)]
pub struct HedwigConfig {
    listen_addr: String,
    application_id: u64,
    discord_token: String,
    test_mode: Option<bool>,

    database: DatabaseConfig,
    logging: Option<LoggingConfig>,
    moderation: ModerationConfig,
    relay: RelayConfig,
    retention: RetentionConfig,
}

impl HedwigConfig {
    pub fn listen_addr(&self) -> &str {
        &self.listen_addr
    }

    pub fn application_id(&self) -> u64 {
        self.application_id
    }

    pub fn discord_token(&self) -> &str {
        &self.discord_token
    }

    pub fn test_mode(&self) -> bool {
        self.test_mode.unwrap_or(false)
    }

    pub fn database(&self) -> &DatabaseConfig {
        &self.database
    }

    pub fn logging(&self) -> Option<&LoggingConfig> {
        self.logging.as_ref()
    }

    pub fn moderation(&self) -> &ModerationConfig {
        &self.moderation
    }

    pub fn relay(&self) -> &RelayConfig {
        &self.relay
    }

    pub fn retention(&self) -> &RetentionConfig {
        &self.retention
    }
}

#[derive(Default, Debug, Clone, Deserialize)]
pub struct DatabaseConfig {
    /// Maximum database connections,
    max_connections: u32,

    /// Database connection string, e.g. postgres://hedwig@127.0.0.1/hedwig
    url: String,
}

impl DatabaseConfig {
    pub fn max_connections(&self) -> u32 {
        self.max_connections
    }
    pub fn url(&self) -> &str {
        &self.url
    }
}

#[derive(Default, Debug, Clone, Deserialize)]
pub struct LoggingConfig {
    /// Discord channel to log message edits/deletions to.
    log_channel: u64,
}

impl LoggingConfig {
    pub fn log_channel(&self) -> ChannelId {
        ChannelId(self.log_channel)
    }
}

#[derive(Default, Debug, Clone, Deserialize)]
pub struct ModerationConfig {
    /// Period between each periodic moderation task (e.g. unbanning).
    polling_period_seconds: i64,
}

impl ModerationConfig {
    pub fn polling_period(&self) -> Duration {
        Duration::seconds(self.polling_period_seconds)
    }
}

#[derive(Default, Debug, Clone, Deserialize)]
pub struct RelayConfig {
    /// Discord channel to relay to frontpage.
    relay_channel: u64,
}

impl RelayConfig {
    pub fn relay_channel(&self) -> ChannelId {
        ChannelId(self.relay_channel)
    }
}

#[derive(Default, Debug, Clone, Deserialize)]
pub struct RetentionConfig {
    /// Whether resync should be skipped on startup
    skip_resync: Option<bool>,

    /// Period between retention checks
    retention_polling_period_seconds: Option<i64>,

    /// Per-channel retention horizons.
    channel_to_retention_seconds: HashMap<ChannelId, i64>,
}

impl RetentionConfig {
    pub fn skip_resync(&self) -> bool {
        self.skip_resync.unwrap_or(false)
    }
    pub fn channel_to_retention(&self) -> HashMap<ChannelId, Duration> {
        self.channel_to_retention_seconds
            .iter()
            .map(|(channel_id, &duration_seconds)| {
                (channel_id.clone(), Duration::seconds(duration_seconds))
            })
            .collect()
    }
    pub fn retention_for_channel(&self, channel_id: &ChannelId) -> Option<Duration> {
        self.channel_to_retention_seconds
            .get(channel_id)
            .map(|&retention_seconds| Duration::seconds(retention_seconds))
    }
    pub fn channel_has_retention_limit(&self, channel_id: &ChannelId) -> bool {
        self.channel_to_retention_seconds.contains_key(channel_id)
    }
    pub fn polling_period(&self) -> Option<Duration> {
        self.retention_polling_period_seconds.map(Duration::seconds)
    }
}

pub fn read_configuration() -> Result<HedwigConfig, config::ConfigError> {
    // Initialise our configuration reader
    let mut settings = config::Config::default();

    // Add configuration values from a file named `configuration`.
    // It will look for any top-level file with an extension
    // that `config` knows how to parse: yaml, json, etc.
    settings.merge(config::File::with_name("config"))?;

    // Try to convert the configuration values it read into
    // our Settings type
    settings.try_into()
}
