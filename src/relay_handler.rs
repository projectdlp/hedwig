use actix::prelude::*;
use log::info;
use std::collections::{HashMap, VecDeque};
use std::time::Duration;

use crate::common::{
    AddSubscriber, RelayMessage, RelayScrollback, RemoveSubscriber, SubscriptionId,
    SubscriptionResponse,
};

use crate::web::SubscriptionSession;

pub struct RelayHandler {
    buffer: VecDeque<RelayMessage>,
    capacity: usize,
    subscribers: Subscribers,
}

impl RelayHandler {
    fn publish(&mut self, message: RelayMessage) {
        info!(
            "Publishing message to {} subscribers: {:?}",
            self.subscribers.len(),
            message
        );

        self.add_to_buffer(message.clone());
        self.subscribers.publish(message)
    }

    fn add_to_buffer(&mut self, message: RelayMessage) {
        if self.buffer.len() == self.capacity {
            self.buffer.pop_front();
        }

        self.buffer.push_back(message);
    }
}

pub struct Subscribers {
    subscribers: HashMap<SubscriptionId, Addr<SubscriptionSession>>,
}

impl Subscribers {
    fn new() -> Subscribers {
        Subscribers {
            subscribers: HashMap::new(),
        }
    }

    fn add(&mut self, subscriber: Addr<SubscriptionSession>) -> SubscriptionId {
        let subscription_id = SubscriptionId::random();

        self.subscribers.insert(subscription_id, subscriber);

        subscription_id
    }

    fn remove(&mut self, subscription_id: SubscriptionId) {
        self.subscribers.remove(&subscription_id);
    }

    fn len(&self) -> usize {
        self.subscribers.len()
    }

    fn publish(&self, message: RelayMessage) {
        self.subscribers
            .values()
            .for_each(|addr| addr.do_send(message.clone()));
    }
}

pub struct RelayHandlerBuilder {
    capacity: usize,
}

impl RelayHandlerBuilder {
    pub fn builder() -> RelayHandlerBuilder {
        RelayHandlerBuilder {
            capacity: 100, // Default
        }
    }

    pub fn scrollback_capacity(mut self, capacity: usize) -> RelayHandlerBuilder {
        self.capacity = capacity;
        self
    }

    pub fn build(self) -> RelayHandler {
        RelayHandler {
            buffer: VecDeque::with_capacity(self.capacity),
            capacity: self.capacity,
            subscribers: Subscribers::new(),
        }
    }
}

impl Actor for RelayHandler {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        ctx.run_interval(Duration::from_secs(60), |act, _| {
            info!("{} subscriptions active.", act.subscribers.len());
        });
    }
}

impl Handler<RelayScrollback> for RelayHandler {
    type Result = ();

    fn handle(&mut self, msg: RelayScrollback, _ctx: &mut Self::Context) {
        self.buffer = msg.0.into_iter().take(self.capacity).collect()
    }
}

impl Handler<RelayMessage> for RelayHandler {
    type Result = ();

    fn handle(&mut self, msg: RelayMessage, _ctx: &mut Self::Context) {
        self.publish(msg);
    }
}

impl Handler<AddSubscriber> for RelayHandler {
    type Result = MessageResult<AddSubscriber>;

    fn handle(&mut self, msg: AddSubscriber, _ctx: &mut Self::Context) -> Self::Result {
        let subscription_id = self.subscribers.add(msg.subscriber);
        info!(
            "Subscription added, {} subscriptions active.",
            self.subscribers.len()
        );

        MessageResult(SubscriptionResponse {
            subscription_id,
            scrollback: self.buffer.iter().cloned().collect(),
        })
    }
}

impl Handler<RemoveSubscriber> for RelayHandler {
    type Result = ();

    fn handle(&mut self, msg: RemoveSubscriber, _ctx: &mut Self::Context) {
        self.subscribers.remove(msg.subscription_id);
        info!(
            "Subscription removed, {} subscriptions active.",
            self.subscribers.len()
        );
    }
}
