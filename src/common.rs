use actix::{Addr, Message};
use chrono::{DateTime, Utc};
use rand::prelude::thread_rng;
use rand::Rng;

use serde::{Deserialize, Serialize};

use crate::web::SubscriptionSession;

pub type Result<T> = std::result::Result<T, failure::Error>;

#[derive(Clone, Debug, Message, Deserialize, Serialize)]
#[rtype(result = "()")]
pub struct RelayMessage {
    pub username: String,
    pub event: Event,
    pub date: DateTime<Utc>,
}

#[allow(non_camel_case_types)]
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(tag = "type")]
pub enum Event {
    STRUCTURED_MESSAGE {
        message_parts: Vec<MessagePart>,
        raw_message: String,
    },
}

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq, Eq)]
#[serde(tag = "part_type")]
pub enum MessagePart {
    String { message: String },
    //  Mention { message: String },
    Emoji { id: u64, name: String, url: String },
}

#[derive(Clone, Copy, Debug, Eq, Hash, Message, PartialEq)]
#[rtype(result = "()")]
pub struct SubscriptionId(pub i64);

impl SubscriptionId {
    pub fn random() -> SubscriptionId {
        SubscriptionId(thread_rng().gen::<i64>())
    }
}

#[derive(Debug)]
pub struct SubscriptionResponse {
    pub subscription_id: SubscriptionId,
    pub scrollback: Vec<RelayMessage>,
}

pub struct AddSubscriber {
    pub subscriber: Addr<SubscriptionSession>,
}

impl Message for AddSubscriber {
    type Result = SubscriptionResponse;
}

#[derive(Debug, Message)]
#[rtype(result = "()")]
pub struct RemoveSubscriber {
    pub subscription_id: SubscriptionId,
}

pub struct RelayScrollback(pub Vec<RelayMessage>);

impl Message for RelayScrollback {
    type Result = ();
}
