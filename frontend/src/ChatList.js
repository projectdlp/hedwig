import Linkify from 'react-linkify';
import React from "react"
import renderHTML from 'react-render-html';
import Moment from 'react-moment';
import "./ChatList.css";

export default ({ chats }) => (
  <ul>
    {chats.map(chat => {
      return (
        <div>
          <div className="chatMessage">
            <div key={chat.id} className="box">
              <Event chat={chat} />
            </div>
          </div>
        </div>
      );
    })}
  </ul>
);

function Event(props) {
  const chat = props.chat;
  const type = chat.event.type.toLowerCase();
  if (type === 'privmsg') {
    return <Message chat={chat} />;
  } else if (type === 'structured_message') {
    return <StructuredMessage chat={chat} />;
  } else if (type === 'action') {
    return <Action chat={chat} />;
  } else if (type === 'join') {
    return <Join chat={chat} />;
  } else if (type === 'part') {
    return <Part chat={chat} />;
  } else if (type === 'quit') {
    return <Quit chat={chat} />;
  }
}

function Message(props) {
  const chat = props.chat;
  return (
    <div>
      <Date>{chat.date}</Date>
      <span className="event-message">
        &lsaquo;<b>{chat.username}</b>&rsaquo; <Linkify>{chat.event.message}</Linkify>
      </span>
    </div>
  )
}

function StructuredMessage(props) {
  const chat = props.chat;
  return (
    <div>
      <Date>{chat.date}</Date>
      <span className="event-message">
        &lsaquo;<b>{chat.username}</b>&rsaquo;&nbsp;
        {chat.event.message_parts.map(part => {
          if (part.part_type === "String") {
            return <Linkify>{part.message}</Linkify>;
          } else if (part.part_type === "Emoji") {
            return <img class="emoji" alt={part.name} src={part.url} />;
          } else {
            return "Wat";
          }
        })}
      </span>
    </div>
  )
}

function Action(props) {
  const chat = props.chat;
  return (
    <div>
      <Date>{chat.date}</Date>
      <span className="event-action">* {chat.username} {chat.event.action}</span>
    </div>
  )
}

function Join(props) {
  const chat = props.chat;
  return (
    <div>
      <Date>{chat.date}</Date>
      <span className="event-join">→ {chat.username} joined.</span>
    </div>
  )
}

function Part(props) {
  const chat = props.chat;
  return (
    <div>
      <Date>{chat.date}</Date>
      <span className="event-part">← {chat.username} left.</span>
    </div>
  )
}

function Quit(props) {
  const chat = props.chat;
  return (
    <div>
      <Date>{chat.date}</Date>
      <span className="event-part">⇐ {chat.username} quit.</span>
    </div>
  )
}

function Date(props) {
  const date = props.children;
  return (
    <span className="event-date">
      <Moment format="HH:mm">{date}</Moment>
    </span>
  )
}

function IrcToHtml(props) {
    const text = props.children;

    // TODO: have this emit react elements for each start/stop.
    //control codes
    const rex = RegExp('\\003([0-9]{1,2})[,]?([0-9]{1,2})?([^\\003]+)', 'g');
    //console.log(rex);

    //console.log(text);
    let lastIndex = 0;
    var elements = [];
    var match;
    let iters = 0;
    while ((match = rex.exec(text)) !== null) {
      if (match.index > lastIndex) {
        elements.push(text.substring(lastIndex, match.index));
      }

      var classes = [];
      if (match[1]) {
        classes.push('fg'+  match[1]);
      }
      if (match[2]) {
        classes.push('bg'+match[1]);
      }

      //console.log(match);
      elements.push(<span className={classes}>{match[3]}</span>);
      lastIndex = match.lastIndex;

      if (iters > 100) {
        return null;
      }
    }
    if (lastIndex < text.length) {
      elements.push(text.substring(lastIndex));
    }

//    if (rex.test(text)) {
//        var cp = null;
//        var cbg = null;
//        while (cp = rex.exec(text)) {
//            if (cp[2]) {
//                cbg = cp[2];
//            }
//            text = text.replace(cp[0],'<span className="fg'+cp[1]+' bg'+cbg+'">'+cp[3]+'</span>');
//        }
//    } else {
//
//    }
    //bold,italics,underline (more could be added.)
//    var bui = [
//        [/\002([^\002]+)(\002)?/, ["<b>","</b>"]],
//        [/\037([^\037]+)(\037)?/, ["<u>","</u>"]],
//        [/\035([^\035]+)(\035)?/, ["<i>","</i>"]]
//    ];
//    for (var i=0;i < bui.length;i++) {
//        var bc = bui[i][0];
//        var style = bui[i][1];
//        if (bc.test(text)) {
//            var bmatch = null;
//            while (bmatch = bc.exec(text)) {
//                text = text.replace(bmatch[0], style[0]+bmatch[1]+style[1]);
//            }
//        }
//    }

    return elements;
}
