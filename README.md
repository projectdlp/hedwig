# Hedwig
Discord bot. Relays output of an Discord channel via websockets. Maintains a scrollback buffer and sends this
buffer to new clients.

## Running

```shell
$ cargo build --release
$ target/release/hedwig
```

## Status

Working.

TBD.

## License
hedwig is primarily distributed under the terms of both the MIT license and the Apache License (Version 2.0),
with portions covered by various BSD-like licenses.

See LICENSE-APACHE, and LICENSE-MIT for details.